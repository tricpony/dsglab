//
//  MockModel.swift
//  TeladocLabTests
//
//  Created by aarthur on 8/31/20.
//  Copyright © 2020 Gigabit LLC. All rights reserved.
//

import Foundation

class MockModel: NSObject, Decodable {
    var name = "name"
    var imageURLString = "url"

    private enum CodingKeys: String, CodingKey {
        case name
        case image
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.imageURLString = try container.decode(String.self, forKey: .image)
    }
}
