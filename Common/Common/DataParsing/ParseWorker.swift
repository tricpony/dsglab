//
//  ParseWorker.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//

import Foundation
import CoreData

/// This protocol is meant to unify the JSONDecoder and PropertyListDecoder classes so that they are indistinguashable to the compiler.
protocol Decoding {
    var userInfo: [CodingUserInfoKey: Any] { get set }
    func decode<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable
}
extension JSONDecoder: Decoding {}
extension PropertyListDecoder: Decoding {}

public enum Format {
    case json
    case plist
}

/// Effectively provides a place to store a managed object context inside the decoder object.
public extension CodingUserInfoKey {
    static let context = CodingUserInfoKey(rawValue: "context")
}

/// Generic struct for parsing json
/// The generic type should be a decodable type that the parser will create and fill
public struct ParseWorker<T: Decodable> {

    /// Parse the payload
    /// - Parameters:
    ///   - payload: JSON data
    ///   - ctx: core data context that is passed to the init of the generic type
    /// - Returns: An array of generic type, T, objects, or nil
    public static func parseData(_ payload: Data?, format: Format, ctx: NSManagedObjectContext? = nil) -> T? {
        func createDecoder() -> Decoding {
            switch format {
            case .json:
                return JSONDecoder()
            case .plist:
                return PropertyListDecoder()
            }
        }
        var decoder = createDecoder()
        if let ctx = ctx, let codingUserInfoContextKey = CodingUserInfoKey.context {
            decoder.userInfo[codingUserInfoContextKey] = ctx
        }
        guard let payload = payload else { return nil }

        do {
            let decoded = try decoder.decode(T.self, from: payload)
            return decoded
        } catch let error {
            print(error)
        }
        return nil
    }
}
