//
//  UserModel.swift
//  Common
//
//  Created by aarthur on 11/22/20.
//

import Foundation

public protocol UserModel where Self: NSObject {
    var firstname: String? { get set }
    var lastname: String? { get set }
    var username: String? { get set }
    var transientConfirmValue: String? { get set }
    var email: String? { get set }
    var password: String? { get set }
    var dept: DepartmentModel? { get set }
    var isNew: Bool { get }
    var userRoles: AnyObject? { get }
    var displayName: String { get }
    var rolesSceneTitle: String { get }
        
    func removeRoles(_ roles: NSSet)
    func removeRole(_ role: RoleModel?)
    func addRole(_ role: RoleModel?)
    func hasRole(_ role: RoleModel?) -> Bool
    func toggleRole(_ role: RoleModel?)
    func validateForSave(confirmPW: Bool) -> String?
}
