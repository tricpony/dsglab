//
//  RoleModel.swift
//  Common
//
//  Created by aarthur on 11/23/20.
//

import Foundation

public protocol RoleModel where Self: NSObject {
    var name: String? { get }
}
