//
//  DepartmentModel.swift
//  Common
//
//  Created by aarthur on 11/22/20.
//

import Foundation

public protocol DepartmentModel where Self: NSObject {
    var name: String? { get }
    var isValid: Bool { get }
}
