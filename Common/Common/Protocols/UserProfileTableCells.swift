//
//  UserProfileTableCells.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit

/// A protocol to unify all table cell types and pass the responsibility of filling in its content.
public protocol UserProfileTableCells where Self: UITableViewCell {
    func fillCell(_ user: UserModel)
}
