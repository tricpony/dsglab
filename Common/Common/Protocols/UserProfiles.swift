//
//  UserProfiles.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit

/// Protocol for view controllers that have a user property
public protocol UserProfiles where Self: UIViewController {
    var user: UserModel! { get set }
}
