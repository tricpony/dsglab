//
//  TextFieldDelegate.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit

/// Serves as a delegate to text fields to populate the properties of a KVC compliant object.
public class TextFieldDelegate: NSObject, UITextFieldDelegate {
    weak var observedObject: NSObject?
    let key: String
    
    public init(observedObject: NSObject?, key: String) {
        self.observedObject = observedObject
        self.key = key
    }
    
    // MARK: UITextFieldDelegate
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        observedObject?.setValue(textField.text, forKey: key)
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
