//
//  Role+CoreDataProperties.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//
//

import Foundation
import CoreData


extension Role {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Role> {
        return NSFetchRequest<Role>(entityName: "Role")
    }

    @NSManaged public var name: String?
    @NSManaged public var id: Int16
    @NSManaged public var departmentId: Int16
    @NSManaged public var department: Department?
    @NSManaged public var user: User?

}

extension Role : Identifiable {

}
