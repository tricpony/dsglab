//
//  Department+CoreDataProperties.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//
//

import Foundation
import CoreData


extension Department {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Department> {
        return NSFetchRequest<Department>(entityName: "Department")
    }

    @NSManaged public var name: String?
    @NSManaged public var valid: Bool
    @NSManaged public var id: Int16
    @NSManaged public var roles: NSSet?
    @NSManaged public var user: User?

}

// MARK: Generated accessors for roles
extension Department {

    @objc(addRolesObject:)
    @NSManaged public func addToRoles(_ value: Role)

    @objc(removeRolesObject:)
    @NSManaged public func removeFromRoles(_ value: Role)

    @objc(addRoles:)
    @NSManaged public func addToRoles(_ values: NSSet)

    @objc(removeRoles:)
    @NSManaged public func removeFromRoles(_ values: NSSet)

}

extension Department : Identifiable {

}
