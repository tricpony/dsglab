//
//  User+CoreDataProperties.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//
//

import Foundation
import CoreData
import Common

extension User {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var firstname: String?
    @NSManaged public var lastname: String?
    @NSManaged public var username: String?
    @NSManaged public var transientConfirmValue: String?
    @NSManaged public var email: String?
    @NSManaged public var password: String?
    @NSManaged public var roles: NSSet?
    @NSManaged public var department: Department?

}

// MARK: Generated accessors for roles
extension User {

    @objc(addRolesObject:)
    @NSManaged public func addToRoles(_ value: Role)

    @objc(removeRolesObject:)
    @NSManaged public func removeFromRoles(_ value: Role)

    @objc(addRoles:)
    @NSManaged public func addToRoles(_ values: NSSet)

    @objc(removeRoles:)
    @NSManaged public func removeFromRoles(_ values: NSSet)

}

extension User : Identifiable {

}
