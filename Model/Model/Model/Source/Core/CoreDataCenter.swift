//
//  CoreDataCenter.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//

import UIKit
import CoreData
import Common

public class CoreDataCenter {
    public static var sharedInstance = CoreDataCenter()
    
    /// Fill database with reference values named department and role.  Only performed on initial launch.
    public func seedDatabase() {
        let previouslyLaunched = UserDefaults.standard.bool(forKey: "previouslyLaunched")
        if !previouslyLaunched {
            let bundle = Bundle(for: Self.self)
            guard let departmentsURL = bundle.url(forResource: "DepartmentValues", withExtension: "plist") else { return }
            guard let rolessURL = bundle.url(forResource: "RoleValues", withExtension: "plist") else { return }
            guard let usersURL = bundle.url(forResource: "UserValues", withExtension: "plist") else { return }
            do {
                let deptPayload = try Data(contentsOf: departmentsURL)
                let rolePayload = try Data(contentsOf: rolessURL)
                let userPayload = try Data(contentsOf: usersURL)
                let departments = ParseWorker<[Department]>.parseData(deptPayload, format: .plist, ctx: mainContext)
                let roles = ParseWorker<[Role]>.parseData(rolePayload, format: .plist, ctx: mainContext)
                let users = ParseWorker<[User]>.parseData(userPayload, format: .plist, ctx: mainContext) ?? [User]()
                var i = 0
                // bind roles to departments where they belong
                departments?.forEach { dept in
                    let matchingRoles = roles?.filter { $0.departmentId == dept.id }
                    matchingRoles?.forEach { role in
                        dept.addToRoles(role)
                    }
                    // assign users to a department
                    if users.count > i {
                        users[i].department = dept
                        i += 1
                    }
                }
                persistContext(mainContext)
                UserDefaults.standard.set(true, forKey: "previouslyLaunched")

            } catch {
                return
            }
        }
    }
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let bundle = Bundle(for: Self.self)
        guard let url = bundle.url(forResource: "DSGLab", withExtension: "momd") else { fatalError("Something has gone horribly wrong.") }
        guard let model = NSManagedObjectModel(contentsOf: url) else { fatalError("Something has gone horribly wrong.") }
        let container = NSPersistentContainer(name: "DSGLab", managedObjectModel: model)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                guard let appDelegate = UIApplication.shared.delegate else { return }
                guard let window = appDelegate.window else { return }
                let rootVC = window?.rootViewController
                rootVC?.presentAlert(title: "Alert", message: "Your data is bad.  App will exit now.", buttonTitles: ["OK"], completion: { (_) in
                    fatalError("Unresolved error \(error), \(error.userInfo)")
                })
            }
        })
        return container
    }()
    public var mainContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    public var childContext: NSManagedObjectContext {
        let ctx = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        ctx.parent = mainContext
        return ctx
    }
    
    // MARK: - Core Data Saving support

    public func persistContext(_ ctx: NSManagedObjectContext, wait: Bool = false) {
        if ctx.hasChanges {
            if wait {
                ctx.performAndWait {
                    do {
                      try ctx.save()
                    } catch let error as NSError {
                      fatalError("Error: \(error.localizedDescription)")
                    }
                }
            } else {
                ctx.perform {
                  do {
                    try ctx.save()
                  } catch let error as NSError {
                    fatalError("Error: \(error.localizedDescription)")
                  }
                }
            }
        }
    }
    
    // MARL: Predicates
    
    public func predicateEqualTo(key: String, value: AnyObject) -> NSPredicate {
        let q = NSComparisonPredicate(leftExpression: NSExpression(forKeyPath: key),
                                      rightExpression: NSExpression(forConstantValue: value),
                                      modifier: .direct,
                                      type: .equalTo)
        return q
    }
}

public extension CoreDataTableViewable {
    var ctx: NSManagedObjectContext {
        return CoreDataCenter.sharedInstance.mainContext
    }
}
