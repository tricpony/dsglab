//
//  Role+CoreDataClass.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//
//

import Foundation
import CoreData
import Common

@objc(Role)
public class Role: NSManagedObject, Decodable, RoleModel {
    @nonobjc public class func orderedFetchRequest(user: UserModel) -> NSFetchRequest<Role> {
        let request = NSFetchRequest<Role>(entityName: "Role")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        request.predicate = CoreDataCenter.sharedInstance.predicateEqualTo(key: "department", value: user.dept as AnyObject)
        return request
    }

    private enum CodingKeys: String, CodingKey {
        case id
        case departmentId
        case name
    }

    public required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoContextKey = CodingUserInfoKey.context,
            let ctx = decoder.userInfo[codingUserInfoContextKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Role", in: ctx) else {
                fatalError("Failed to decode Role")
        }
        self.init(entity: entity, insertInto: ctx)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.id = try container.decode(Int16.self, forKey: .id)
        self.departmentId = try container.decode(Int16.self, forKey: .departmentId)
    }
}
