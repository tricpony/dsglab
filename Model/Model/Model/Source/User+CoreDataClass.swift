//
//  User+CoreDataClass.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//
//

import Foundation
import CoreData
import Common

private enum ValidationKeys: String {
    case field
    case error
}

private enum ValidationErrors: String {
    case firstname = "Please enter first name."
    case lastname = "Please enter last name."
    case email = "Please enter email."
    case username = "Please enter username."
    case password = "Please enter password."
    case nomatch = "Password confirmation does not match."
}

@objc(User)
public class User: NSManagedObject, Decodable, UserModel {
    @nonobjc public class func orderedFetchRequest() -> NSFetchRequest<User> {
        let request = NSFetchRequest<User>(entityName: "User")
        let sortDescriptors = [NSSortDescriptor(key: "lastname", ascending: true), NSSortDescriptor(key: "firstname", ascending: true)]
        request.sortDescriptors = sortDescriptors
        return request
    }

    public var displayName: String {
        let lastname = self.lastname ?? ""
        let firstname = self.firstname ?? ""
        return "\(lastname), \(firstname)"
    }
    
    public var rolesSceneTitle: String {
        let name = self.department?.name ?? ""
        return "\(name) Roles"
    }
    
    public var isNew: Bool {
        return objectID.isTemporaryID;
    }

    private enum CodingKeys: String, CodingKey {
        case firstname
        case lastname
        case email
        case password
        case username
    }

    public required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoContextKey = CodingUserInfoKey.context,
            let ctx = decoder.userInfo[codingUserInfoContextKey] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "User", in: ctx) else {
                fatalError("Failed to decode User")
        }
        self.init(entity: entity, insertInto: ctx)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.firstname = try container.decode(String.self, forKey: .firstname)
        self.lastname = try container.decode(String.self, forKey: .lastname)
        self.username = try container.decode(String.self, forKey: .username)
        self.password = try container.decode(String.self, forKey: .password)
        self.email = try container.decode(String.self, forKey: .email)
    }
    
    public func hasRole(_ role: RoleModel?) -> Bool {
        guard let role = role as? Role else { return false }
        return roles?.contains(role) ?? false
    }
    
    public func toggleRole(_ role: RoleModel?) {
        if hasRole(role) {
            removeRole(role)
        } else {
            addRole(role)
        }
    }
        
    /// Validate required fields.  Password  is only confirmed when confirmPW
    /// is true and either the user is new or password has changed.
    /// - Parameters:
    ///   - field: Name of the attribute in question;.
    public func validateForSave(confirmPW: Bool) -> String? {
        let fieldInfo = [[ValidationKeys.field.rawValue: firstname ?? "", ValidationKeys.error.rawValue: ValidationErrors.firstname.rawValue],
                         [ValidationKeys.field.rawValue: lastname ?? "", ValidationKeys.error.rawValue: ValidationErrors.lastname.rawValue],
                         [ValidationKeys.field.rawValue: email ?? "", ValidationKeys.error.rawValue: ValidationErrors.email.rawValue],
                         [ValidationKeys.field.rawValue: username ?? "", ValidationKeys.error.rawValue: ValidationErrors.username.rawValue],
                         [ValidationKeys.field.rawValue: password ?? "", ValidationKeys.error.rawValue: ValidationErrors.password.rawValue]]
        var i = 0
        
        while i < fieldInfo.count {
            let info = fieldInfo[i]
            let error = info[ValidationKeys.error.rawValue]
            guard let field = info[ValidationKeys.field.rawValue] else {
                return error
            }
            if field.isEmpty {
                return error
            }
            i += 1
        }
        if confirmPW && (isNew || passwordIsDirty) {
            guard let confirmField = transientConfirmValue else {
                return ValidationErrors.nomatch.rawValue
            }
            if confirmField.isEmpty {
                return ValidationErrors.nomatch.rawValue
            }
        }
        return nil  // returning nil implies that validation passed
    }
    
    /// Determine if a attribute has changed.  Only works for string attributes.
    /// - Parameters:
    ///   - field: Name of the attribute in question;.
    private func hasChangesFor(field: String) -> Bool {
        if hasChanges {
            if changedValues()[field] == nil {
                return false
            }
            return true
        }
        return false
    }
    
    /// Determine if password differs from its persisted value in the database.
    var passwordIsDirty: Bool {
        return hasChangesFor(field: "password")
    }
    
    // MARK: UserModel
    
    public var userRoles: AnyObject? {
        return roles
    }
    
    public var dept: DepartmentModel? {
        get {
            return department
        }
        set {
            department = newValue as? Department
        }
    }
    
    public func removeRoles(_ roles: NSSet) {
        removeFromRoles(roles)
    }
    
    public func removeRole(_ role: RoleModel?) {
        guard let role = role as? Role else { return }
        removeFromRoles(role)
    }
    
    public func addRole(_ role: RoleModel?) {
        guard let role = role as? Role else { return }
        addToRoles(role)
    }
}
