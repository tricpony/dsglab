//
//  Department+CoreDataClass.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//
//

import Foundation
import CoreData
import Common

@objc(Department)
public class Department: NSManagedObject, Decodable, DepartmentModel {
    @nonobjc public class func orderedFetchRequest() -> NSFetchRequest<Department> {
        let request = NSFetchRequest<Department>(entityName: "Department")
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        return request
    }

    public var isValid: Bool {
        return valid
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case valid
    }

    public required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoContextKey = CodingUserInfoKey.context else { fatalError("Failed to decode Department") }
        guard let ctx = decoder.userInfo[codingUserInfoContextKey] as? NSManagedObjectContext else { fatalError("Failed to decode Department") }
        guard let entity = NSEntityDescription.entity(forEntityName: "Department", in: ctx) else { fatalError("Failed to decode Department") }
        self.init(entity: entity, insertInto: ctx)
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.id = try container.decode(Int16.self, forKey: .id)
        self.valid = try container.decode(Bool.self, forKey: .valid)
    }
}
