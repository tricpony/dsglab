//
//  UsersViewController.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//

import UIKit
import CoreData
import Common
import Model
import Content

/// This view controller presents the initial landing screen.
class UsersViewController: UIViewController, CoreDataTableViewable {
    @IBOutlet weak var tableView: UITableView!
    lazy var fetchedResultsController: NSFetchedResultsController<User> = {
        NSFetchedResultsController(fetchRequest: User.orderedFetchRequest(),
                                   managedObjectContext: ctx,
                                   sectionNameKeyPath: nil,
                                   cacheName: "Users")
    }()
    
    func loadNavButtons() {
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(performAdd))
        navigationItem.rightBarButtonItem = addButton
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadNavButtons()
        title = "Users"
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshFetchedObjects()
    }
    
    @objc
    func performAdd() {
        _ = NSEntityDescription.insertNewObject(forEntityName: "User", into: ctx)
    }

    func refreshFetchedObjects() {
        fetchedResultsController.delegate = self
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch {
            print("Fetch failed")
        }
    }
    
    // MARK: - Table View

    func cellIdentifier(at indexPath: IndexPath) -> String {
        return "User"
    }

    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(at: indexPath)) else {
            return UITableViewCell.init(style: .default, reuseIdentifier: self.cellIdentifier(at: indexPath))
        }
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let user = fetchedResultsController.object(at: indexPath)
            ctx.delete(user)
            CoreDataCenter.sharedInstance.persistContext(ctx)
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = nextCellForTableView(tableView, at: indexPath)
        let user = fetchedResultsController.object(at: indexPath)
        cell.textLabel?.text = user.displayName
        return cell
    }
    
    // MARK: NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            tableView.insertRows(at: [newIndexPath], with: .right)
            tableView.selectRow(at: newIndexPath, animated: false, scrollPosition: .none)
            performSegue(withIdentifier: "showDetail", sender: nil)
        case .delete:
            guard let indexPath = indexPath else { return }
            tableView.deleteRows(at: [indexPath], with: .left)
        default:
            break
        }
    }
    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let user = fetchedResultsController.object(at: indexPath)
                guard let userProfileViewController = segue.destination as? UserProfileViewController else { return }
                userProfileViewController.user = user
            }
        }
    }
}
