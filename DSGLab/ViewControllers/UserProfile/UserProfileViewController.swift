//
//  UserProfileViewController.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit
import Common
import Model

class UserProfileViewController: UIViewController, UserProfiles {
    var user: UserModel!
    var didSave = false
    let ctx = CoreDataCenter.sharedInstance.mainContext

    func loadNavButtons() {
        let saveButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(performSave))
        navigationItem.rightBarButtonItem = saveButton
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadNavButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        didSave = false
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil, didSave == false {
            view.endEditing(true)
            ctx.rollback()
        }
    }
    
    @objc
    func performSave() {
        view.endEditing(true)
        let validationResults = user.validateForSave(confirmPW: true)
        if validationResults == nil {
            didSave = true
            CoreDataCenter.sharedInstance.persistContext(ctx)
            navigationController?.popViewController(animated: true)
            return
        }
        presentAlert(title: "Alert", message: validationResults, buttonTitles: ["OK"], completion: nil)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let controller = segue.destination as? UserProfiles else { return }
        controller.user = user
    }
}
