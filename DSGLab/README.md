# README #

Read below some comments about what you will find in DSGLab.

### Language ###
The project was written in Swift.

### Best user experience ###
The app works best on an iPhone.  In the simulator the password and confirm fields get hung up on autofill but everything else runs fine.

### Anomalies ###
A runtime warning named UITableViewAlertForLayoutOutsideViewHierarchy will appear in the debug console at unpredictable times.  This appears to be a bug in iOS 13 and so far I have not found a fix.  See these links for others who find the same warning:
https://developer.apple.com/forums/thread/120790
https://www.hackingwithswift.com/forums/swiftui/problems-with-tabview/111

### Modules ###
Illustrates how to separate reusable logic out to be shared across many projects.

### Storyboards ###
Storyboards are used generously.  Every scene is defined in separate storyboards with segues on storyboard references.

### Core Data ###
Core data is used to persist application data.  Raw Department and role data is loaded from plists on the initial launch.

### View Containers ###
The user profile scene is composed of a master view controller holding several containers with underlying child view controllers.

### Unit Tests ###
Some unit tests have been included.
