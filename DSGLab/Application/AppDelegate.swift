//
//  AppDelegate.swift
//  DSGLab
//
//  Created by aarthur on 11/11/20.
//

import UIKit
import Model

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let sb: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = sb.instantiateInitialViewController()
        window?.rootViewController = rootVC
        CoreDataCenter.sharedInstance.seedDatabase()
        return true
    }
}
