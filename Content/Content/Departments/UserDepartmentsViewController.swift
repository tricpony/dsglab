//
//  UserDepartmentsViewController.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit
import CoreData
import Common
import Model

public class UserDepartmentsViewController: UIViewController, UserProfiles, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var pickerView: UIPickerView!
    var ctx = CoreDataCenter.sharedInstance.mainContext
    public var user: UserModel!
    lazy var fetchedResultsController: NSFetchedResultsController<Department> = {
        NSFetchedResultsController(fetchRequest: Department.orderedFetchRequest(),
                                   managedObjectContext: ctx,
                                   sectionNameKeyPath: nil,
                                   cacheName: "Department")
    }()

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshFetchedObjects()
        scrollPickerToSelectedDept()
    }

    func scrollPickerToSelectedDept() {
        guard let userDeptName = user.dept?.name else { return }
        guard let fetchedObjects = fetchedResultsController.fetchedObjects else { return }
        guard let fetchedObject = fetchedObjects.filter({ $0.name == userDeptName }).first else { return }
        guard let selectdRow = fetchedObjects.firstIndex(of: fetchedObject) else { return }
        pickerView.selectRow(selectdRow, inComponent: 0, animated: false)
    }
    
    func refreshFetchedObjects() {
        do {
            try fetchedResultsController.performFetch()
            pickerView.reloadAllComponents()
        } catch {
            print("Fetch failed")
        }
    }

    // MARK: - Picker View
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![component]
        return sectionInfo.numberOfObjects
    }

    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dept = fetchedResultsController.object(at: IndexPath(row: row, section: component))
        return dept.name
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dept = fetchedResultsController.object(at: IndexPath(row: row, section: component))
        guard let roles = user.userRoles as? NSSet else { return }
        guard dept.isValid else {
            user.dept = nil
            user.removeRoles(roles)
            return
        }
        user.dept = dept
        user.removeRoles(roles)
    }
}
