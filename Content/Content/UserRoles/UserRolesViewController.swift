//
//  UserRolesViewController.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit
import CoreData
import Common
import Model

class UserRolesViewController: UIViewController, UserProfiles, CoreDataTableViewable {
    @IBOutlet weak var tableView: UITableView!
    var user: UserModel!
    lazy var fetchedResultsController: NSFetchedResultsController<Role> = {
        let request = NSFetchRequest<Role>(entityName: "Role")
        let sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        request.sortDescriptors = sortDescriptors
        request.predicate = CoreDataCenter.sharedInstance.predicateEqualTo(key: "user", value: user as AnyObject)
        return NSFetchedResultsController(fetchRequest: request,
                                          managedObjectContext: ctx,
                                          sectionNameKeyPath: nil,
                                          cacheName: nil)
    }()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshFetchedObjects()
    }

    func refreshFetchedObjects() {
        fetchedResultsController.delegate = self
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch {
            print("Fetch failed")
        }
    }

    // MARK: - Table View

    func cellIdentifier(at indexPath: IndexPath) -> String {
        return "Role"
    }

    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(at: indexPath)) else {
            return UITableViewCell.init(style: .default, reuseIdentifier: self.cellIdentifier(at: indexPath))
        }
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = nextCellForTableView(tableView, at: indexPath)
        let user = fetchedResultsController.object(at: indexPath)
        cell.textLabel?.text = user.name
        return cell
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let controller = segue.destination as? UserProfiles else { return }
        controller.user = user
    }
    
    // MARK: NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { return }
            tableView.insertRows(at: [newIndexPath], with: .right)
        case .update:
            guard let indexPath = indexPath else { return }
            tableView.reloadRows(at: [indexPath], with: .none)
        case .delete:
            guard let indexPath = indexPath else { return }
            tableView.deleteRows(at: [indexPath], with: .left)
        default:
            break
        }
    }
}
