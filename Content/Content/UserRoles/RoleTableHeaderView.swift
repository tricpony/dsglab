//
//  RoleTableHeaderView.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit
import Common

public class RoleTableHeaderView: UITableViewController, UserProfiles {
    public var user: UserModel!

    // MARK: - Table View

    func cellIdentifier(at indexPath: IndexPath) -> String {
        return "Header"
    }

    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(at: indexPath)) else {
            return UITableViewCell.init(style: .default, reuseIdentifier: self.cellIdentifier(at: indexPath))
        }
        return cell
    }

    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = nextCellForTableView(tableView, at: indexPath)
        cell.textLabel?.text = "User Roles"
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard user.dept != nil else {
            presentAlert(title: "Alert", message: "Please select a Department.", buttonTitles: ["OK"], completion: nil)
            return
        }
        performSegue(withIdentifier: "showRoles", sender: nil)
    }

    // MARK: - Segues

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let controller = segue.destination as? UserProfiles else { return }
        controller.user = user
    }
}
