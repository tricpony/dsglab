//
//  PasswordTableViewCell.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit
import Common

class PasswordTableViewCell: UITableViewCell, UserProfileTableCells {
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var confirmField: UITextField!
    var passwordDelegate: TextFieldDelegate!
    var confirmDelegate: TextFieldDelegate!

    override func prepareForReuse() {
        passwordField.delegate = nil
        confirmField.delegate = nil
    }
    
    func assignTextFieldDelegate(_ user: UserModel) {
        passwordDelegate = TextFieldDelegate(observedObject: user, key: "password")
        confirmDelegate = TextFieldDelegate(observedObject: user, key: "transientConfirmValue")
        passwordField.delegate = passwordDelegate
        confirmField.delegate = confirmDelegate
    }

    func fillCell(_ user: UserModel) {
        passwordField.text = user.password
        assignTextFieldDelegate(user)
    }
}
