//
//  CredentialsTableViewCell.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit
import Common

class EmailTableViewCell: UITableViewCell, UserProfileTableCells {
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    var emailDelegate: TextFieldDelegate!
    var usernameDelegate: TextFieldDelegate!
    
    override func prepareForReuse() {
        emailField.delegate = nil
        usernameField.delegate = nil
    }
    
    func assignTextFieldDelegate(_ user: UserModel) {
        emailDelegate = TextFieldDelegate(observedObject: user, key: "email")
        usernameDelegate = TextFieldDelegate(observedObject: user, key: "username")
        emailField.delegate = emailDelegate
        usernameField.delegate = usernameDelegate
    }

    func fillCell(_ user: UserModel) {
        emailField.text = user.email
        usernameField.text = user.username
        assignTextFieldDelegate(user)
    }
}
