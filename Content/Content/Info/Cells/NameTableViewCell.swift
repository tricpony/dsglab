//
//  NameTableViewCell.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit
import Common
import Model

class NameTableViewCell: UITableViewCell, UserProfileTableCells {
    @IBOutlet weak var firstnameField: UITextField!
    @IBOutlet weak var lastnameField: UITextField!
    var firstnameDelegate: TextFieldDelegate!
    var lastnameDelegate: TextFieldDelegate!
    
    override func prepareForReuse() {
        firstnameField.delegate = nil
        lastnameField.delegate = nil
    }
    
    func assignTextFieldDelegate(_ user: UserModel) {
        firstnameDelegate = TextFieldDelegate(observedObject: user, key: "firstname")
        lastnameDelegate = TextFieldDelegate(observedObject: user, key: "lastname")
        firstnameField.delegate = firstnameDelegate
        lastnameField.delegate = lastnameDelegate
    }
    
    func fillCell(_ user: UserModel) {
        firstnameField.text = user.firstname
        lastnameField.text = user.lastname
        assignTextFieldDelegate(user)
        if user.isNew {
            firstnameField.becomeFirstResponder()
        }
    }
}
