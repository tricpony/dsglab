//
//  UserInfoViewController.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit
import Common

enum ReuseIdentifier: String, CaseIterable {
    case Name, Email, Password
}

public class UserInfoViewController: UIViewController, UserProfiles, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    public var user: UserModel!

    // MARK: - Table View

    func cellIdentifier(at indexPath: IndexPath) -> String {
        return ReuseIdentifier.allCases[indexPath.row].rawValue
    }

    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> UserProfileTableCells? {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(at: indexPath)) as? UserProfileTableCells
        return cell
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ReuseIdentifier.allCases.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = nextCellForTableView(tableView, at: indexPath) else { return UITableViewCell() }
        cell.fillCell(user)
        
        return cell
    }
}
