//
//  RolesViewController.swift
//  DSGLab
//
//  Created by aarthur on 11/12/20.
//

import UIKit
import CoreData
import Common
import Model

class RolesViewController: UIViewController, UserProfiles, CoreDataTableViewable {
    @IBOutlet weak var tableView: UITableView!
    var user: UserModel!
    lazy var fetchedResultsController: NSFetchedResultsController<Role> = {
        NSFetchedResultsController(fetchRequest: Role.orderedFetchRequest(user: user),
                                   managedObjectContext: ctx,
                                   sectionNameKeyPath: nil,
                                   cacheName: nil)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = user.rolesSceneTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshFetchedObjects()
    }
    
    func refreshFetchedObjects() {
        fetchedResultsController.delegate = self
        do {
            try fetchedResultsController.performFetch()
            tableView.reloadData()
        } catch {
            print("Fetch failed")
        }
    }

    // MARK: - Table View

    func cellIdentifier(at indexPath: IndexPath) -> String {
        return "Role"
    }

    func nextCellForTableView(_ tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(at: indexPath)) else {
            return UITableViewCell.init(style: .default, reuseIdentifier: self.cellIdentifier(at: indexPath))
        }
        return cell
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = nextCellForTableView(tableView, at: indexPath)
        let role = fetchedResultsController.object(at: indexPath)
        cell.textLabel?.text = role.name
        if user.hasRole(role) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let role = fetchedResultsController.object(at: indexPath)
        user.toggleRole(role)
    }
    
    // MARK: NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    // This triggers a runtime warning
    // Tried many fixes, nothing worked
    // I suspect it is a bug introduced in iOS 13
    // See this for more
    // https://developer.apple.com/forums/thread/120790
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch type {
        case .update:
            guard let indexPath = indexPath else { return }
            tableView.reloadRows(at: [indexPath], with: .none)
        default:
            break
        }
    }
}
